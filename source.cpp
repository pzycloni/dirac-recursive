#include <iostream>

using namespace std;

pair<long long int, long long int> Dirac(int number) {

	if (number < 1) {
		return make_pair(0, 0);
	}

	if (number == 1) {
		return make_pair(1, 0);
	}

	auto result = Dirac((number + 1) / 2);

	if (number % 2 == 0) {
		result.second += result.first;
		return result;
	}
	else {
		result.first += result.second;
		return result;
	}
}

int main() {
	
	cout << Dirac(10).first << " " << Dirac(10).second << endl;
	cin.get();

	return 0;

}